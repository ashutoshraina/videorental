package com.thoughtworks.videorental.domain;

import org.joda.time.Period;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.*;

public class ClassicMovieChargingStrategyTest {

    private ClassicMovieChargingStrategy pricingStrategy;

    @Before
    public void setup(){
        pricingStrategy = new ClassicMovieChargingStrategy();
    }

    @Test
    public void chargeForAClassicMovieIsHalfDollarPerDayTill6Days() {
        assertEquals(2, pricingStrategy.getPrice(4), 0.0);
    }

    @Test
    public void chargeForAClassicMovieAWeekIs$3() {
        assertEquals(3, pricingStrategy.getPrice(7), 0.0);
    }

    @Test
    public void chargeForAClassicMovieIsHalfDollarPerDayAfterAWeek() {
        assertEquals(3.5, pricingStrategy.getPrice(8), 0.0);

        assertEquals(4.5, pricingStrategy.getPrice(10), 0.0);
    }

}