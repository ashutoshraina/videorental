package com.thoughtworks.videorental.domain;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.*;

public class NewMovieChargingStrategyTest {
    private NewMovieChargingStrategy pricingStrategy;

    @Before
    public void setup() {
        pricingStrategy = new NewMovieChargingStrategy();
    }

    @Test
    public void chargeForANewMovieFor5DaysShouldBe9Dollars() {
        assertEquals(9, pricingStrategy.getPrice(5), 0.0);
    }

    @Test
    public void chargeForANewMovieFor1DayShouldBe3Dollars() {
        assertEquals(3, pricingStrategy.getPrice(1), 0.0);
    }


}