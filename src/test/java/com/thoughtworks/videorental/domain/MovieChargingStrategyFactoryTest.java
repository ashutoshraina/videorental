package com.thoughtworks.videorental.domain;

import org.junit.Test;

import static org.junit.Assert.*;

public class MovieChargingStrategyFactoryTest {

    @Test
    public void returnDefaultMoviePricingStrategyForDefaultMovie() {
        MovieChargingStrategy chargingStrategy = MovieChargingStrategyFactory.getStrategyFor(MovieType.DEFAULT);
        assertEquals(DefaultMoviePricingStrategy.class, chargingStrategy.getClass());
    }

    @Test
    public void returnNewMoviePricingStrategyForNewMovie() {
        MovieChargingStrategy chargingStrategy = MovieChargingStrategyFactory.getStrategyFor(MovieType.NEW);
        assertEquals(NewMovieChargingStrategy.class, chargingStrategy.getClass());
    }

    @Test
    public void returnClassicMoviePricingStrategyForClassicMovie() {
        MovieChargingStrategy chargingStrategy = MovieChargingStrategyFactory.getStrategyFor(MovieType.CLASSIC);
        assertEquals(ClassicMovieChargingStrategy.class, chargingStrategy.getClass());
    }
}