package com.thoughtworks.videorental.domain;

import org.joda.time.LocalDateTime;
import org.joda.time.Period;
import org.junit.Test;

import java.util.Date;

import static junit.framework.Assert.assertEquals;

public class RentalTest {
    private Customer customer = new Customer("Blah");
    private LocalDateTime rentedOn = LocalDateTime.fromDateFields(new Date());

    @Test
    public void chargeForADefaultMovieIs1DollarADay() throws Exception {

        Movie movie = new Movie("Hugh", MovieType.DEFAULT);
        Period period = Period.days(5);

        Rental rental = new Rental(customer, movie, period, rentedOn);

        //When
        Double charge = rental.getCharge();

        //Then
        assertEquals(5, charge, 0.0);
    }

    @Test
    public void chargeForANewMovieFor5DaysShouldBe9Dollars() throws Exception {

        Movie movie = new Movie("Hugh", MovieType.NEW);
        Period period = Period.days(5);

        Rental rental = new Rental(customer, movie, period, rentedOn);

        //When
        Double charge = rental.getCharge();

        //Then
        assertEquals(9, charge, 0.0);
    }

    @Test
    public void chargeForANewMovieFor1DayShouldBe3Dollars() throws Exception {

        Movie movie = new Movie("Hugh", MovieType.NEW);
        Period period = Period.days(1);

        Rental rental = new Rental(customer, movie, period, rentedOn);

        //When
        Double charge = rental.getCharge();

        //Then
        assertEquals(3, charge, 0.0);
    }

    @Test
    public void chargeForAClassicMovieIsHalfDollarPerDayTill6Days() {
        Movie movie = new Movie("Hugh", MovieType.CLASSIC);

        Period period = Period.days(4);
        Rental rental = new Rental(customer, movie, period, rentedOn);
        assertEquals(2, rental.getCharge(), 0.0);

    }

    @Test
    public void chargeForAClassicMovieAWeekIs$3() {
        Movie movie = new Movie("Hugh", MovieType.CLASSIC);

        Period period = Period.days(7);
        Rental rental = new Rental(customer, movie, period, rentedOn);
        assertEquals(3, rental.getCharge(), 0.0);
    }

    @Test
    public void chargeForAClassicMovieIsHalfDollarPerDayAfterAWeek() {
        Movie movie = new Movie("Hugh", MovieType.CLASSIC);

        Period period = Period.days(8);
        Rental rental = new Rental(customer, movie, period, rentedOn);
        assertEquals(3.5, rental.getCharge(), 0.0);

        period = Period.days(10);
        rental = new Rental(customer, movie, period, rentedOn);
        assertEquals(4.5, rental.getCharge(), 0.0);
    }

    @Test
    public void rentalForNewMovieIs2PointsPerDay() {
        Movie movie = new Movie("Hugh", MovieType.NEW);

        Period period = Period.days(8);
        Rental rental = new Rental(customer, movie, period, rentedOn);
        assertEquals(16, rental.getPoints());

    }

    @Test
    public void rentalForMoviesExceptNewIs1PointPer$() {
        Movie movie = new Movie("Hugh", MovieType.CLASSIC);

        Period period = Period.days(9);
        Rental rental = new Rental(customer, movie, period, rentedOn);
        assertEquals(4, rental.getPoints());
    }

    @Test
    public void truncatePointsToNearestIntegerWhenFloatingPointNumber() {
        Movie movie = new Movie("Hugh", MovieType.CLASSIC);

        Period period = Period.days(8);
        Rental rental = new Rental(customer, movie, period, rentedOn);
        assertEquals(3, rental.getPoints());
    }
}