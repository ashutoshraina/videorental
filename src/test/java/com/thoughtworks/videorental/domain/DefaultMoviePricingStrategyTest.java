package com.thoughtworks.videorental.domain;

import org.junit.Test;

import static org.junit.Assert.*;

public class DefaultMoviePricingStrategyTest {

    @Test
    public void chargeHalfDollarPerDay() {
        DefaultMoviePricingStrategy pricingStrategy = new DefaultMoviePricingStrategy();
        assertEquals(4.0, pricingStrategy.getPrice(4), 0.0);
    }

}