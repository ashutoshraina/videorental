package com.thoughtworks.videorental.domain;

import lombok.EqualsAndHashCode;
import org.joda.time.LocalDateTime;
import org.joda.time.Period;

@EqualsAndHashCode
public class Rental {
    private final Movie movie;
    private final Customer customer;
    private final LocalDateTime rentedOn;
    private final Period period;

    public Rental(Customer customer, Movie movie, Period period, LocalDateTime rentedOn) {
        this.movie = movie;
        this.customer = customer;
        this.period = period;
        this.rentedOn = rentedOn;
    }

    public Movie getMovie() {
        return movie;
    }

    public Customer getCustomer() {
        return customer;
    }

    public Period getPeriod() {
        return period;
    }

    public LocalDateTime getRentedOn() {
        return rentedOn;
    }

    public LocalDateTime getEndDate() {
        return rentedOn.plus(period);
    }

    public Double getCharge() {
        int days = period.getDays();
        MovieChargingStrategy chargingStrategy = MovieChargingStrategyFactory.getStrategyFor(movie.getType());
        return chargingStrategy.getPrice(days);
    }

    public int getPoints() {
        if(movie.getType() == MovieType.NEW){
            return period.getDays() * 2;
        }
        return getCharge().intValue();
    }
}