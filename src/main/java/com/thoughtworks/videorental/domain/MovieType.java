package com.thoughtworks.videorental.domain;

/**
 * Created by ashutoshraina on 12/02/15.
 */
public enum MovieType {
    DEFAULT, CLASSIC, NEW
}
