package com.thoughtworks.videorental.domain;

public class MovieChargingStrategyFactory {

    public static MovieChargingStrategy getStrategyFor(MovieType type) {
        switch (type) {
            case CLASSIC:
                return new ClassicMovieChargingStrategy();
            case NEW:
                return new NewMovieChargingStrategy();
            case DEFAULT:
            default:
                return new DefaultMoviePricingStrategy();
        }
    }
}
