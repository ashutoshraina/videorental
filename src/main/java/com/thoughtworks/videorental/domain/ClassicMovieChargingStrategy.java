package com.thoughtworks.videorental.domain;

public class ClassicMovieChargingStrategy implements MovieChargingStrategy {
    @Override
    public Double getPrice(int days) {
        double perDayCharge = 0.5;
        double weeklyCharge = 3.0;

        if (days < 7) {
            return days * perDayCharge;
        }
        if (days == 7) {
            return weeklyCharge;
        }
        return perDayCharge + getPrice(days - 1);

    }
}
