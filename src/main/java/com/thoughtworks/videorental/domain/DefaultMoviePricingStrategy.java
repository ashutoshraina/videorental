package com.thoughtworks.videorental.domain;

public class DefaultMoviePricingStrategy implements MovieChargingStrategy {
    @Override
    public Double getPrice(int days) {
        return days * 1.0;
    }
}
