package com.thoughtworks.videorental.domain;

public class NewMovieChargingStrategy implements MovieChargingStrategy {
    @Override
    public Double getPrice(int days) {
        Double totalCharge = 0.0;

        if (days < 2) {
            totalCharge += 3;
        } else {
            totalCharge += 3 + extendedPeriod(days) * 2;
        }

        return totalCharge;
    }
    private int extendedPeriod(int days) {
        return days - 2;
    }

}
