package com.thoughtworks.videorental.domain;

public interface MovieChargingStrategy {
    Double getPrice(int days);
}
